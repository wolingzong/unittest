package com.rude3knife.commonutils.utils;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;

import org.springframework.scheduling.annotation.EnableAsync;

/**
 * @author leon
 */
@SpringBootApplication
@EnableAsync // 启动异步调用
public class TestServiceApiApplication {

    public static void main(String[] args) {

        SpringApplication.run(TestServiceApiApplication.class, args);
    }
}
